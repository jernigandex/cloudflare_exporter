module gitlab.com/gitlab-org/cloudflare_exporter

go 1.14

require (
	github.com/alecthomas/units v0.0.0-20210208195552-ff826a37aa15 // indirect
	github.com/cespare/xxhash/v2 v2.1.2 // indirect
	github.com/go-kit/kit v0.11.0
	github.com/go-logfmt/logfmt v0.5.1 // indirect
	github.com/machinebox/graphql v0.2.2
	github.com/matryer/is v1.4.0 // indirect
	github.com/oklog/run v1.1.0
	github.com/prometheus/client_golang v1.11.0
	github.com/prometheus/common v0.30.0
	github.com/prometheus/procfs v0.7.3 // indirect
	github.com/stretchr/testify v1.7.0
	golang.org/x/sys v0.0.0-20210908233432-aa78b53d3365 // indirect
	gopkg.in/alecthomas/kingpin.v2 v2.2.6
)
