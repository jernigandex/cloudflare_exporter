package main

import (
	"context"
	"os"
	"path/filepath"
	"sync"
	"testing"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/testutil"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

// HACK: should really make metricsMaxAge injectable, but that would require a
// bit of refactoring that doesn't quite need to be done. For now, override
// metricsMaxAge to a very large number so that the hardcoded bucket times in
// the test fixtures do not become stale.
func init() {
	metricsMaxAge = time.Hour * 24 * 365 * 100
}

func TestParseZoneIDs(t *testing.T) {
	for _, tc := range []struct {
		name          string
		zonesFilter   []string
		expectedZones map[string]string
	}{
		{
			name:          "returns map of all non-pending zones when no filter specified",
			expectedZones: map[string]string{"zone-1-id": "zone-1", "zone-2-id": "zone-2"},
		},
		{
			name:          "returns map of non-pending zones that are also present in filter",
			zonesFilter:   []string{"zone-2"},
			expectedZones: map[string]string{"zone-2-id": "zone-2"},
		},
	} {
		t.Run(tc.name, func(t *testing.T) {
			f, err := os.Open("testdata/zones_resp.json")
			require.Nil(t, err)
			defer f.Close()
			zones, err := parseZoneIDs(f, tc.zonesFilter)
			require.Nil(t, err)
			assert.Equal(t, zones, tc.expectedZones)
		})
	}
}

func TestZoneAnalytics(t *testing.T) {
	for _, testCase := range []struct {
		name                       string
		metricsUnderTest           []string
		lastUpdatedTime            string
		apiRespFixturePaths        []string
		expectedMetricsFixturePath string
	}{
		{
			// This test is kept from the previous version as a regression test.
			name: "Firewall events regression test",
			metricsUnderTest: []string{
				"cloudflare_zones_firewall_events_total",
			},
			lastUpdatedTime:            "2020-02-06T10:01:00Z",
			apiRespFixturePaths:        []string{"firewall_events_resp.json"},
			expectedMetricsFixturePath: "expected_firewall_events.metrics",
		},
		{
			// This test is kept from the previous version as a regression test.
			name: "Health check regression test",
			metricsUnderTest: []string{
				"cloudflare_zones_health_check_events_total",
			},
			lastUpdatedTime:            "2020-02-06T10:01:00Z",
			apiRespFixturePaths:        []string{"health_check_events_resp.json"},
			expectedMetricsFixturePath: "expected_health_check_events.metrics",
		},
		{
			// This test case is a quick, comprehensive test, if the exporter exports metrics as designed.
			name: "end to end test",
			metricsUnderTest: []string{
				"cloudflare_zones_firewall_events_total",
				"cloudflare_zones_health_check_events_total",
				"cloudflare_zones_http_cached_bytes_total",
				"cloudflare_zones_http_cached_requests_total",
				"cloudflare_zones_http_colo_bytes_total",
				"cloudflare_zones_http_colo_requests_total",
				"cloudflare_zones_http_country_bytes_total",
				"cloudflare_zones_http_country_requests_total",
				"cloudflare_zones_http_protocol_requests_total",
				"cloudflare_zones_http_responses_total",
				"cloudflare_zones_http_tls_requests_total",
				"cloudflare_zones_ip_reputation_requests_total",
			},
			lastUpdatedTime:            "2020-02-06T10:01:00Z",
			apiRespFixturePaths:        []string{"end_to_end.json"},
			expectedMetricsFixturePath: "end_to_end.metrics",
		},
		{
			// This tests, that overlapping sampled data, is properly added to the same metric.
			//
			// The test data returns the same data, with only the sampling interval being different.
			// The metrics should show the cumulative count in one metric.
			name: "Overlapping sampling",
			metricsUnderTest: []string{
				"cloudflare_zones_firewall_events_total",
				"cloudflare_zones_http_cached_bytes_total",
				"cloudflare_zones_http_cached_requests_total",
				"cloudflare_zones_http_colo_bytes_total",
				"cloudflare_zones_http_colo_requests_total",
				"cloudflare_zones_http_country_bytes_total",
				"cloudflare_zones_http_country_requests_total",
				"cloudflare_zones_http_protocol_requests_total",
				"cloudflare_zones_http_responses_total",
				"cloudflare_zones_http_tls_requests_total",
			},
			lastUpdatedTime:            "2020-02-06T10:01:00Z",
			apiRespFixturePaths:        []string{"overlapping_sampling.json"},
			expectedMetricsFixturePath: "overlapping_sampling.metrics",
		},
		{
			// This tests, network error log reporting.
			//
			// This test has no test-case as we do not have an example from the API.
			name: "Network error logging",
			metricsUnderTest: []string{
				"cloudflare_zones_network_error_logs_total",
			},
			lastUpdatedTime:            "2020-02-06T10:01:00Z",
			apiRespFixturePaths:        []string{"nel.json"},
			expectedMetricsFixturePath: "nel.metrics",
		},
	} {
		t.Run(testCase.name, func(t *testing.T) {
			reg := prometheus.NewPedanticRegistry()
			registerMetrics(reg)

			lastUpdatedTime, err := time.Parse(time.RFC3339, testCase.lastUpdatedTime)
			require.Nil(t, err)

			// add 1 Minutes to adjust for internal bias of the TimeBucket
			lastUpdatedTime = lastUpdatedTime.Add(time.Minute)

			cfExporter := exporter{
				logger:        newPromLogger("error"),
				scrapeLock:    &sync.Mutex{},
				graphqlClient: newFakeGraphqlClient(testCase.apiRespFixturePaths),
				scrapeBucket: newTimeBucket(
					lastUpdatedTime,
					time.Minute,
					time.Minute,
				),
				extractors: map[string]extractFunc{
					"graphql:zones:trafficCached":                extractZoneHTTPCached,
					"graphql:zones:trafficCountry":               extractZoneHTTPCountry,
					"graphql:zones:trafficColo":                  extractZoneHTTPColo,
					"graphql:zones:trafficDetails":               extractZoneHTTPDetailed,
					"graphql:zones:networkErrorLogs":             extractZoneNetworkErrorLogs,
					"graphql:zones:firewallEventsAdaptiveGroups": extractZoneFirewallEvents,
					"graphql:zones:reputation":                   extractZoneIPReputation,
					"graphql:zones:healthCheckEventsGroups":      extractZoneHealthCheckEvents,
				},
			}
			zones := map[string]string{"a-zone": "a-zone-name"}
			require.Nil(t, cfExporter.getZoneAnalytics(context.Background(), zones))

			fixture, err := os.Open(filepath.Join("testdata", testCase.expectedMetricsFixturePath))
			require.Nil(t, err)
			defer fixture.Close()

			// This error is formatted much nicer using stdlib testing.
			err = testutil.GatherAndCompare(reg, fixture, testCase.metricsUnderTest...)
			if err != nil {
				t.Fatal(err)
			}
		})
	}
}
